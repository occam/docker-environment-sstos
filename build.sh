# UPDATE APT CACHE TO POINT TO ARCHIVE
apt-get update

echo 'y' | apt-get install gcc make g++ zlib1g-dev libssl-dev libbz2-dev git wget bzip2 udev vim libsqlite3-dev subversion mercurial libtool libtool-bin libconfig++-dev config++-bin unzip
echo 'y' | apt-get install mpic++ autoconf libltdl-dev

# PYTHON
git clone git://github.com/yyuu/pyenv.git /pyenv

echo 'ln -s /proc/self/fd /dev/fd'  >> /etc/bash.bashrc
echo 'export PYENV_ROOT=/pyenv'     >> /etc/bash.bashrc
echo 'export PATH=$PYENV_ROOT/bin:$PATH' >> /etc/bash.bashrc
echo 'eval "$(pyenv init -)"'       >> /etc/bash.bashrc

# INSTALL PYTHONS
echo 'export PYENV_ROOT=/pyenv'     >> /root/pyenv.sh
echo 'export PATH=/pyenv/bin:$PATH' >> /root/pyenv.sh
echo 'eval "$(pyenv init -)"'       >> /root/pyenv.sh

# 2.7.6
#echo 'pyenv install 2.7.6'          >> /root/pyenv.sh

# 3.3.0
#echo 'pyenv install 3.3.0'          >> /root/pyenv.sh

/bin/bash /root/pyenv.sh

echo 'y' | apt-get install mpic++ autoconf

# Create installation path
mkdir -p /opt/local
mkdir -p /opt/local/lib
mkdir -p /opt/local/packages

########   REMOVE IN FUTURE? ###########
export PYENV_ROOT=/pyenv
export PATH=$PYENV_ROOT/bin:$PATH
eval "$(pyenv init -)"
########   REMOVE IN FUTURE? ###########


# 2.7.6
env PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install 2.7.6 -f



pyenv local 2.7.6
export CPATH=$CPATH:`python-config --prefix`/include
export LIBRARY_PATH=$LIBRARY_PATH:`python-config --prefix`/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`python-config --prefix`/lib
echo "export LIBRARY_PATH=\$LIBRARY_PATH:`python-config --prefix`/lib" >> /etc/bash.bashrc
echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:`python-config --prefix`/lib" >> /etc/bash.bashrc

# Get OpenMPI 1.8

# Determine install path for OpenMPI
export MPIHOME=/opt/local/packages/OpenMPI-1.8
echo 'export MPIHOME=/opt/local/packages/OpenMPI-1.8'       >> /etc/bash.bashrc

# Check if MPI is already built
if [ ! -e MPI_BUILT ]; then
  # Unpack
  if [ ! -f openmpi-1.8 ]; then
    tar -xvf openmpi-1.8.tar.gz

    # Build OpenMPI
    cd openmpi-1.8
    ./configure --prefix=$MPIHOME
    make all
    make install
    INSTALL_SUCCESS=$?
    cd ..
    # Make sure make install succeded
    if [ $INSTALL_SUCCESS -eq 0 ]
    then
     touch MPI_BUILT
     rm -rf openmpi-1.8.tar.gz openmpi-1.8
    fi
  fi
else
  echo "Skipping build openmpi ... Already built"
fi

#Update PATH
export PATH=$MPIHOME/bin:$PATH
export MPICC=mpicc
export MPICXX=mpicxx
echo 'export PATH=$MPIHOME/bin:$PATH'  >> /etc/bash.bashrc
echo 'export MPICC=mpicc'              >> /etc/bash.bashrc
echo 'export MPICXX=mpicxx'            >> /etc/bash.bashrc



# Get Boost 1.56

# Determine install path for Boost
export BOOST_HOME=/opt/local/packages/boost-1.56
echo 'export BOOST_HOME=/opt/local/packages/boost-1.56'            >> /etc/bash.bashrc

# Check if Boost is already built
if [ ! -e BOOST_BUILT ]; then

  # Unpack
  if [ ! -f boost_1_56_0 ]; then
    tar -xvf boost_1_56_0.tar.gz

    # Build Boost
    cd boost_1_56_0
    ./bootstrap.sh --prefix=$BOOST_HOME
    ./b2 cxxflags=-std=c++11 install
    INSTALL_SUCCESS=$?
    cd ..
    # Make sure make install succeded
    if [ $INSTALL_SUCCESS -eq 0 ]
    then
     touch BOOST_BUILT
     rm -rf boost_1_56_0.tar.gz boost_1_56_0
    fi
  fi
else
  echo "Skipping build Boost ... Already built"
fi
pyenv local 2.7.6
export CPATH=$CPATH:`python-config --prefix`/include
export LIBRARY_PATH=$LIBRARY_PATH:`python-config --prefix`/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`python-config --prefix`/lib

export MPIHOME=/opt/local/packages/OpenMPI-1.8
export BOOST_HOME=/opt/local/packages/boost-1.56

# Compile DRAMSIM2 for building SST
export DRAMSIM2_HOME=/opt/dramsim2
echo "export DRAMSIM2_HOME=$DRAMSIM2_HOME"  >> /etc/bash.bashrc
# Check if Boost is already built
if [ ! -e DRAMSIM2_BUILT ]; then

    # Build SST
    cd dramsim2
    make libdramsim.so
    MAKE_SUCCESS=$?
    cd ..
    # Make sure make install succeded
    if [ $MAKE_SUCCESS -eq 0 ]
    then
        touch DRAMSIM2_BUILT
    fi
else
    echo "Skipping build DRAMSIM2 ... Already built"
fi


#Install SST-core
export SST_HOME_CORE=/opt/local/sst-core
echo 'export SST_HOME_CORE=/opt/local/sst-core'  >> /etc/bash.bashrc
echo 'export PATH=$SST_HOME_CORE/bin:$PATH'  >> /etc/bash.bashrc

echo 'export SST_PKGCONFIG_PATH=$SST_HOME_CORE/lib/pkgconfig'  >> /etc/bash.bashrc
echo 'export PKG_CONFIG_PATH=${SST_PKGCONFIG_PATH}:${PKG_CONFIG_PATH}'  >> /etc/bash.bashrc

# Check if SST Core is already built
if [ ! -e SST_CORE_BUILT ]; then

    # Build SST
    cd sst-core
    git checkout de33ca83d4f10a9f223ede0eca2408ce20bc228e
    git apply ../7.1.patch
    pyenv local 2.7.6
    if [ ! -e ./configure ]; then
        ./autogen.sh
    fi
    if [ ! -e ./Makefile ]; then
        ./configure --prefix=$SST_HOME_CORE
    fi
    make all -j2
    make install
    INSTALL_SUCCESS=$?
    cd ..
    # Make sure make install succeded
    if [ $INSTALL_SUCCESS -eq 0 ]
    then
        touch SST_CORE_BUILT
        #rm -rf sst-core
    fi
else
    echo "Skipping build SST Core ... Already built"
fi

export SST_HOME_ELEMENTS=/opt/local/sst-elements
echo 'export SST_HOME_ELEMENTS=/opt/local/sst-elements'  >> /etc/bash.bashrc
echo 'export PATH=$SST_HOME_ELEMENTS/bin:$PATH'  >> /etc/bash.bashrc

# Check if SST Elements is already built
if [ ! -e SST_ELEMENTS_BUILT ]; then

    # Build SST
    cd sst-elements
	git checkout 85eccf3b4690f0e66bb3a0202dcd079a87fc07f5
    pyenv local 2.7.6
    if [ ! -e ./configure ]; then
        ./autogen.sh
    fi
    if [ ! -e ./Makefile ]; then
        ./configure --prefix=$SST_HOME_ELEMENTS --with-sst-core=$SST_HOME_CORE --with-dramsim=$DRAMSIM2_HOME 
    fi
    make all -j2
    make install
    INSTALL_SUCCESS=$?
    cd ..
    # Make sure make install succeded
    if [ $INSTALL_SUCCESS -eq 0 ]
    then
        touch SST_ELEMENTS_BUILT
        mkdir -p sst-git/sst/elements/simpleElementExample/tests/
        cp sst-elements/src/sst/elements/simpleElementExample/tests/test_simpleMessageGeneratorComponent.py sst-git/sst/elements/simpleElementExample/tests/test_simpleMessageGeneratorComponent.py
        #rm -rf sst-elements
    fi
else
    echo "Skipping build SST Core ... Already built"
fi
